Comando para compilar el Hello world
====================================

Desde el directorio raiz del proyecto:
`docker run -v $PWD:/app -w /app demo/oracle-java:8 javac src/HelloWorld.java`


Comando para ejecutar el Hello world desde docker
==================================================

Desde dentro del directorio src del proyecto:
`docker run --rm -v $PWD:/app -w /app demo/oracle-java:8 java HelloWorld`


